// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import { basename, dirname, extname } from 'path'
import * as vscode from 'vscode'

interface Rule {
	to: string
	transform: (fileName: string) => string
}

const extName = 'et-sya-related-files'

const rules: Rule[] = [
	{
		to: 'Controller', transform: (fileName) => `${kebabCaseToCamelCaps(extractName(fileName))}Controller.ts`
	},
	{
		to: 'Helper', transform: (fileName) => `${kebabCaseToCamelCaps(extractName(fileName))}Helper.ts`
	},
	{
		to: 'Service', transform: (fileName) => `${kebabCaseToCamelCaps(extractName(fileName))}Service.ts`
	},
	{
		to: 'Nunjuck view', transform: (fileName) => `${extractName(fileName)}.njk`
	},
	{
		to: 'English locale', transform: (fileName) => `*/en/translation/${extractName(fileName)}.json`
	},
	{
		to: 'Welsh locale', transform: (fileName) => `*/cy/translation/${extractName(fileName)}.json`
	},
	{
		to: 'Controller test', transform: (fileName) => `${kebabCaseToCamelCaps(extractName(fileName))}Controller.test.ts`
	},
	{
		to: 'Helper test', transform: (fileName) => `${kebabCaseToCamelCaps(extractName(fileName))}Helper.test.ts`
	},
	{
		to: 'Service test', transform: (fileName) => `${kebabCaseToCamelCaps(extractName(fileName))}Service.test.ts`
	},
	{
		to: 'Route test', transform: (fileName) => `*/routes/${extractName(fileName)}.test.ts`
	},
	{
		to: 'View test', transform: (fileName) => `*/views/${extractName(fileName)}.test.ts`
	}
]

// This method is called when your extension is deactivated
export function deactivate() { }

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const config = vscode.workspace.getConfiguration(extName)
	const excludeGlobs = config.get('excludeGlobs') as string[]

	let openRelatedCommand = vscode.commands.registerCommand(`${extName}.openRelated`, () => openRelated(excludeGlobs))

	context.subscriptions.push(openRelatedCommand)
}

export async function openRelated(excludeGlobs: string[]) {
	const editor = vscode.window.activeTextEditor
	if (!editor) {
		return vscode.window.showInformationMessage('No file is open!')
	}

	const currentFileNameNoExt = getActiveFileNameNoExt(editor.document.fileName)
	const activeWorkspaceFolder = vscode.workspace.getWorkspaceFolder(editor.document.uri)

	if (!activeWorkspaceFolder) {
		return vscode.window.showInformationMessage(`Couldn't find workspace folder for ${currentFileNameNoExt}`)
	}

	const files = await getFilesMatchingRules(rules, currentFileNameNoExt, activeWorkspaceFolder, excludeGlobs)
	const getAlias = (type: string, uri?: vscode.Uri) => `${type}: ${uri?.path.replace(activeWorkspaceFolder.uri.path, '') || '<no file found>'}`

	const options = files.filter(o => o.value).map(o => { return { file: o.value, alias: getAlias(o.type, o.value) } })

	const selected = await vscode.window.showQuickPick(options.map(o => o.alias), { placeHolder: 'Select a file type' })
	if (!selected) {
		return
	}

	const file = options.find(o => o.alias === selected)?.file
	if (!file) {
		return vscode.window.showErrorMessage(`Couldn't find file for ${selected} (this is a bug)`)
	}

	const doc = await vscode.workspace.openTextDocument(file)
	await vscode.window.showTextDocument(doc, { preview: false })
}

export function getActiveFileNameNoExt(file: string) {
	const currentFileName = basename(file)
	const currentFileExt = extname(currentFileName)
	const currentFileNameNoExt = currentFileName.substring(0, currentFileName.length - currentFileExt.length)

	return currentFileNameNoExt
}

export async function getFilesMatchingRules(rules: Rule[], currentFileNameNoExt: string, activeWorkspaceFolder: vscode.WorkspaceFolder, excludeGlobs: string[] = []) {
	const files = await Promise.allSettled(rules.flatMap(rule => {
		const targetFileName = rule.transform(currentFileNameNoExt)
		const relativePattern = new vscode.RelativePattern(activeWorkspaceFolder, `**/${caseInsensitiveGlob(targetFileName)}`)
		return vscode.workspace.findFiles(relativePattern, `{${excludeGlobs.join(',')}}`)
	}))

	return files.flatMap((o, i) => (o.status === 'fulfilled' ? [{ type: rules[i].to, value: o.value[0] }] : []))
}

export function caseInsensitiveGlob(str: string) {
	return str.split('').map(char => {
		if (char >= 'a' && char <= 'z') {
			return `[${char.toUpperCase()}${char}]`
		} else if (char >= 'A' && char <= 'Z') {
			return `[${char}${char.toLowerCase()}]`
		} else {
			return char
		}
	}).join('')
}

export function camelCapsToKebabCase(str: string) {
	return str
		.replace(/([a-z0-9])([A-Z])/g, '$1-$2')
		.replace(/([A-Z])([A-Z][a-z])/g, '$1-$2')
		.toLowerCase()
}

export function kebabCaseToCamelCaps(str: string) {
	return str.replace(/-([a-z])/g, (_, p1) => p1.toUpperCase()).replace(/^./, str => str.toUpperCase())
}

/**
 * Extracts the unique name part of the file excluding any file extensions and returns in kebab-case.
 * Eg, path/to/acas-cert-num.test.ts -> acas-cert-num
 * Eg, path/to/AcasCertNumController.test.ts -> acas-cert-num
 */
export function extractName(str: string) {
	const base = basename(str)
	const noExt = base.replace(/\.[^.]+$/, '')
	const noQualifiers = noExt.replace(/\.test$/, '').replace(/(Controller|Service|Helper)$/, '')

	if (noQualifiers.includes('-')) {
		return noQualifiers
	}

	return camelCapsToKebabCase(noQualifiers)
}
