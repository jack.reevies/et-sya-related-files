import * as assert from 'assert'

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode'
import { camelCapsToKebabCase, caseInsensitiveGlob, extractName, getActiveFileNameNoExt, getFilesMatchingRules, kebabCaseToCamelCaps, openRelated } from '../../extension'
import { mock } from 'node:test'
// import * as myExtension from '../../extension'

const mockUri = (file: string) => { return { path: file, fsPath: file, scheme: '', authority: '', 'fragment': '', 'query': '', toJSON: () => file, with: (change: vscode.Uri) => change } }

const mockFindFileResults = [
	[mockUri('src/main/controllers/AcasCertNumController.ts')],
	[mockUri('src/main/controllers/helpers/AcasCertNumHelper.ts')],
	[mockUri('src/main/services/AcasCertNumService.ts')],
	[mockUri('src/main/views/acas-cert-num.njk')],
	[mockUri('src/main/resources/locales/en/translation/acas-cert-num.json')],
	[mockUri('src/main/resources/locales/cy/translation/acas-cert-num.json')],
	[mockUri('src/test/unit/controller/AcasCertNumController.test.ts')],
	[mockUri('src/test/unit/controller/helpers/AcasCertNumHelper.test.ts')],
	[mockUri('src/test/unit/service/AcasCertNumService.test.ts')],
	[mockUri('src/test/routes/acas-cert-num.test.ts')],
	[mockUri('src/test/unit/views/acas-cert-num.test.ts')]
]

suite('Extension Test Suite', () => {
	vscode.window.showInformationMessage('Start all tests.')

	const extractNameData = [
		{ src: 'src/main/resources/locales/cy/translation/acas-cert-num.json', expected: 'acas-cert-num' },
		{ src: 'src/main/controllers/AcasCertNumController.ts', expected: 'acas-cert-num' },
		{ src: 'src/main/views/acas-cert-num.njk', expected: 'acas-cert-num' },
		{ src: 'src/test/unit/views/acas-cert-num.test.ts', expected: 'acas-cert-num' },
		{ src: 'src/test/routes/acas-cert-num.test.ts', expected: 'acas-cert-num' },
		{ src: 'src/test/unit/controller/AcasCertNumController.test.ts', expected: 'acas-cert-num' },
		{ src: 'src/main/controllers/helpers/AcasCertNumHelper.ts', expected: 'acas-cert-num' },
		{ src: 'src/main/services/AcasCertNumService.ts', expected: 'acas-cert-num' },
		{ src: 'src/main/resources/locales/cy/translation/benefits.json', expected: 'benefits' },
		{ src: 'src/main/controllers/BenefitsController.ts', expected: 'benefits' },
		{ src: 'src/main/views/benefits.njk', expected: 'benefits' },
		{ src: 'src/test/unit/views/benefits.test.ts', expected: 'benefits' },
		{ src: 'src/test/routes/benefits.test.ts', expected: 'benefits' },
		{ src: 'src/test/unit/controller/BenefitsController.test.ts', expected: 'benefits' },
		{ src: 'src/main/controllers/helpers/BenefitsHelper.ts', expected: 'benefits' },
		{ src: 'src/main/services/BenefitsService.ts', expected: 'benefits' },
	]

	extractNameData.forEach(({ src, expected }) => {
		test(`extractName should return "${expected}" for ${src}`, () => {
			assert.strictEqual(extractName(src), expected)
		})
	})

	const caseInsensitiveGlobData = [
		{ src: 'Rule92Controller', expected: '[Rr][Uu][Ll][Ee]92[Cc][Oo][Nn][Tt][Rr][Oo][Ll][Ll][Ee][Rr]' },
		{ src: 'acas-cert-num', expected: '[Aa][Cc][Aa][Ss]-[Cc][Ee][Rr][Tt]-[Nn][Uu][Mm]' },
		{ src: 'ContactTheTribunalCYAHelper', expected: '[Cc][Oo][Nn][Tt][Aa][Cc][Tt][Tt][Hh][Ee][Tt][Rr][Ii][Bb][Uu][Nn][Aa][Ll][Cc][Yy][Aa][Hh][Ee][Ll][Pp][Ee][Rr]' },
	]

	caseInsensitiveGlobData.forEach(({ src, expected }) => {
		test(`caseInsensitiveGlob should return "${expected}" for ${src}`, () => {
			assert.strictEqual(caseInsensitiveGlob(src), expected)
		})
	})

	const camelCapsToKebabCaseData = [
		{ src: 'Rule92Controller', expected: 'rule92-controller' },
		{ src: 'acas-cert-num', expected: 'acas-cert-num' },
		{ src: 'ContactTheTribunalCYAHelper', expected: 'contact-the-tribunal-cya-helper' },
	]

	camelCapsToKebabCaseData.forEach(({ src, expected }) => {
		test(`camelCapsToKebabCase should return "${expected}" for ${src}`, () => {
			assert.strictEqual(camelCapsToKebabCase(src), expected)
		})
	})

	const kebabCaseToCamelCapsData = [
		{ src: 'rule92-controller', expected: 'Rule92Controller' },
		{ src: 'acas-cert-num', expected: 'AcasCertNum' },
		{ src: 'contact-the-tribunal-cya-helper', expected: 'ContactTheTribunalCyaHelper' },
	]

	kebabCaseToCamelCapsData.forEach(({ src, expected }) => {
		test(`kebabCaseToCamelCaps should return "${expected}" for ${src}`, () => {
			assert.strictEqual(kebabCaseToCamelCaps(src), expected)
		})
	})

	const getActiveFileNameNoExtData = [
		{ src: 'src/main/resources/locales/cy/translation/acas-cert-num.json', expected: 'acas-cert-num' },
		{ src: 'src/main/controllers/AcasCertNumController.ts', expected: 'AcasCertNumController' },
		{ src: 'src/main/views/acas-cert-num.njk', expected: 'acas-cert-num' },
		{ src: 'src/test/unit/views/acas-cert-num.test.ts', expected: 'acas-cert-num.test' },
		{ src: 'src/test/routes/acas-cert-num.test.ts', expected: 'acas-cert-num.test' },
		{ src: 'src/test/unit/controller/AcasCertNumController.test.ts', expected: 'AcasCertNumController.test' },
		{ src: 'src/main/controllers/helpers/AcasCertNumHelper.ts', expected: 'AcasCertNumHelper' },
		{ src: 'src/main/services/AcasCertNumService.ts', expected: 'AcasCertNumService' }
	]

	getActiveFileNameNoExtData.forEach(({ src, expected }) => {
		test(`getActiveFileNameNoExt should return "${expected}" for ${src}`, () => {
			assert.strictEqual(getActiveFileNameNoExt(src), expected)
		})
	})

	test('should return a list of files matching the rules', async () => {

		let count = 0
		vscode.workspace.findFiles = () => {
			return Promise.resolve(mockFindFileResults[count++])
		}

		const mockRules = [
			{ to: 'Controller', transform: (_: string) => 'AcasCertNumController.ts' },
			{ to: 'Helper', transform: (_: string) => 'AcasCertNumHelper.ts' },
			{ to: 'Service', transform: (_: string) => 'AcasCertNumService.ts' },
			{ to: 'Nunjuck view', transform: (_: string) => 'acas-cert-num.njk' },
			{ to: 'English locale', transform: (_: string) => 'en/translation/acas-cert-num.json' },
			{ to: 'Welsh locale', transform: (_: string) => 'cy/translation/acas-cert-num.json' },
			{ to: 'Controller test', transform: (_: string) => 'AcasCertNumController.test.ts' },
			{ to: 'Helper test', transform: (_: string) => 'AcasCertNumHelper.test.ts' },
			{ to: 'Service test', transform: (_: string) => 'AcasCertNumService.test.ts' },
			{ to: 'Route test', transform: (_: string) => 'routes/acas-cert-num.test.ts' },
			{ to: 'View test', transform: (_: string) => 'views/acas-cert-num.test.ts' },
		]

		const results = await getFilesMatchingRules(mockRules, 'acas-cert-num', { uri: mockUri(''), index: 0, name: 'lol' }, [])

		assert.strictEqual(results.length, 11)

		const expectedResults = [
			{ type: 'Controller', fsPath: 'src/main/controllers/AcasCertNumController.ts' },
			{ type: 'Helper', fsPath: 'src/main/controllers/helpers/AcasCertNumHelper.ts' },
			{ type: 'Service', fsPath: 'src/main/services/AcasCertNumService.ts' },
			{ type: 'Nunjuck view', fsPath: 'src/main/views/acas-cert-num.njk' },
			{ type: 'English locale', fsPath: 'src/main/resources/locales/en/translation/acas-cert-num.json' },
			{ type: 'Welsh locale', fsPath: 'src/main/resources/locales/cy/translation/acas-cert-num.json' },
			{ type: 'Controller test', fsPath: 'src/test/unit/controller/AcasCertNumController.test.ts' },
			{ type: 'Helper test', fsPath: 'src/test/unit/controller/helpers/AcasCertNumHelper.test.ts' },
			{ type: 'Service test', fsPath: 'src/test/unit/service/AcasCertNumService.test.ts' },
			{ type: 'Route test', fsPath: 'src/test/routes/acas-cert-num.test.ts' },
			{ type: 'View test', fsPath: 'src/test/unit/views/acas-cert-num.test.ts' }
		]

		expectedResults.forEach((expectedResult, index) => {
			assert.strictEqual(results[index].type, expectedResult.type)
			assert.strictEqual(results[index].value.fsPath, expectedResult.fsPath)
		})

	})

	test('openRelated should exit early when no active file is open', async () => {
		let testMessage = ''
		vscode.window.showInformationMessage = (message: string, ..._: any[]) => {
			testMessage = message
			return Promise.resolve()
		}
		await openRelated([])
		assert.strictEqual(testMessage, 'No file is open!')
	})

	test('openRelated should exit early when no active workspace folder is open', async () => {
		Object.defineProperty(vscode, 'window', { value: { activeTextEditor: { document: { fileName: 'mock' } } } })

		let testMessage = ''
		vscode.window.showInformationMessage = (message: string, ..._: any[]) => {
			testMessage = message
			return Promise.resolve()
		}
		await openRelated([])
		assert.strictEqual(testMessage, `Couldn't find workspace folder for mock`)
	})

	test('openRelated should correctly identify the active file and open the related files', async () => {
		Object.defineProperty(vscode, 'window', {
			value: {
				// Simulate src/main/views/acas-cert-num.njk as the currently open document
				activeTextEditor: { document: { fileName: 'src/main/views/acas-cert-num.njk' } },
				// Simulate user selecting the related Controller to this nunjuck view
				showQuickPick: () => Promise.resolve('Controller: src/main/controllers/AcasCertNumController.ts'),
				// Log any calls to showInformationMessage for debugging - there shouldn't be any if the test is set up correctly
				showInformationMessage: (message: string) => console.log(message),
				// Record the final file that the method requested to open
				showTextDocument: (file: vscode.Uri) => { openFileRequest = file }
			}
		})

		Object.defineProperty(vscode, 'workspace', {
			value: {
				// Used to construct the relative pattern
				getWorkspaceFolder: () => { return { uri: mockUri('et-sya-frontend'), index: 0, name: 'et-sya-frontend' } },
				// This would read the file and transform into a document - but we don't need this for our test
				openTextDocument: (file: vscode.Uri) => Promise.resolve(file)
			}
		})

		let count = 0
		vscode.workspace.findFiles = () => {
			// Load in the mocked search results one by one
			return Promise.resolve(mockFindFileResults[count++])
		}

		// This variable will be written to on the final `openTextDocument` call
		let openFileRequest: vscode.Uri
		// Function under test
		await openRelated([])
		// Ensure that the request file is indeed the one we simulated selecting earlier
		assert.strictEqual(openFileRequest!.fsPath, 'src/main/controllers/AcasCertNumController.ts')
	})
})
