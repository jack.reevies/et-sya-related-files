# et-sya-related-files README

Quickly jump to other related files through the command palette. The extension currently recognises:

File | Matcher
-|-
Controller | \*/\*ExampleTypeControler.ts
Helper | \*/\*ExampleTypeHelper.ts
Service | \*/\*ExampleTypeService.ts
Nunjuck View | \*/example-type.njk
EN Locale | \*/en/translation/example-type.json
CY Locale | \*/cy/translation/example-type.json
Controller Test | \*ExampleTypeController.test.ts
Helper Test | \*ExampleTypeHelper.test.ts
Service Test | \*ExampleTypeService.test.ts
Route Test | \*/routes/example-type.test.ts
View Test | \*/views/example-type.test.ts

From a file, open the comamnd palette (CTRL + SHIFT + P or F1) and type "Go to related file". A list of found related files will be shown along with their locations. Selecting one will open it in a new tab

![et-sya-related-files](https://gitlab.com/jack.reevies/et-sya-related-files/-/blob/master/et-sya-related-files.gif)

## Requirements

VSCode 1.81.0 or higher as defined in the package.json however it can probably work on lower versions as the extension doesn't make heavy use of VSCode's API

## Extension Settings

Include if your extension adds any VS Code settings through the `contributes.configuration` extension point.

For example:

This extension contributes the following settings:

* `et-sya-related-files.excludeGlobs`: A string array of glob patterns to exclude from the file search when looking up related files.

Defaults are most generated folders and dot hidden folders `["\*\*/node_modules/\*\*", "\*\*/bower_components/\*\*", "\*\*/dist/\*\*", "\*\*/build/\*\*", "\*\*/out/\*\*", "\*\*/target/\*\*", "\*\*/.\*/\*\*"]`

## Known Issues

Please raise an issue on gitlab/hub if you find an issue or have a suggestion

## Release Notes

### 1.0.0

Initial release - Supports:

File | Matcher
-|-
Controller | \*/\*ExampleTypeControler.ts
Helper | \*/\*ExampleTypeHelper.ts
Service | \*/\*ExampleTypeService.ts
Nunjuck View | \*/example-type.njk
EN Locale | \*/en/translation/example-type.json
CY Locale | \*/cy/translation/example-type.json
Controller Test | \*ExampleTypeController.test.ts
Helper Test | \*ExampleTypeHelper.test.ts
Service Test | \*ExampleTypeService.test.ts
Route Test | \*/routes/example-type.test.ts
View Test | \*/views/example-type.test.ts

### Contributing

Contributions are welcome, the initial release of this extension really is just MVP.

Run tests with `npm test` - you may need to be on node v20 as we're using built in test runners with no external deps

**Enjoy!**
