# Change Log

All notable changes to the "et-sya-related-files" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

### [1.0.0] - 2023-08-25

Initial release - Supports:

File | Matcher
-|-
Controller | \*/\*ExampleTypeControler.ts
Helper | \*/\*ExampleTypeHelper.ts
Service | \*/\*ExampleTypeService.ts
Nunjuck View | \*/example-type.njk
EN Locale | \*/en/translation/example-type.json
CY Locale | \*/cy/translation/example-type.json
Controller Test | \*ExampleTypeController.test.ts
Helper Test | \*ExampleTypeHelper.test.ts
Service Test | \*ExampleTypeService.test.ts
Route Test | \*/routes/example-type.test.ts
View Test | \*/views/example-type.test.ts